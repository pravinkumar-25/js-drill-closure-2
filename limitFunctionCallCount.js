function limitFunctionCallCount(cb,n){
    return function(value,array){
        for(let i=0;i<n;i++){
            array.push(cb(value,i));
        }
        return array;
    }
};
module.exports = limitFunctionCallCount;