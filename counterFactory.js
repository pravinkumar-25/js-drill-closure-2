function counterFactory(){
    return {
        increment: function(value){
            return value+1;
        },
        decrement: function(value){
            return value-1;
        }
    }
}
module.exports = counterFactory;