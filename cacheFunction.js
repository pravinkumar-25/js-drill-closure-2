function cacheFunction(cb){
    let obj ={};
    return function(...element){
        if(obj[element]){
            return obj;
        }else{
            obj[element] = cb(element);
            return obj;
        }
        
    }
}
module.exports = cacheFunction;