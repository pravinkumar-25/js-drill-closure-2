const cacheFunction = require("../cacheFunction");

const cb =(elements)=>{
    let sum=0;
    for(let i=0;i< elements.length;i++){
        sum += elements[i];
    }
    return sum;
};

let returnFunction = cacheFunction(cb);

console.log(returnFunction(1,2,5));
console.log(returnFunction(1,3,4,2,1));