const limitFunctionCallCount = require("../limitFunctionCallCount");

function callBack(value,index){
    return (value*value)+index;
}

let returnFunction = limitFunctionCallCount(callBack,5);
let array =returnFunction(10,[]);

console.log(array)
